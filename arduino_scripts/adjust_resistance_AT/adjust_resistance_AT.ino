// this script combines sound, moisture and light test
#include <Adafruit_NeoPixel.h>
int plant_pin = 1;
int plant_read = 1; // pin 2 = A1
int plant_value = 0;
int sound_out = 0;

int i;

void setup() {
  // put your setup code here, to run once:
  pinMode(plant_pin, OUTPUT);
  //Serial.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly:
  digitalWrite(plant_pin, HIGH);
  delay(500);
  plant_value = analogRead(plant_read);
  plant_value = plant_value/100;

  for (i = 0; i <= plant_value; i++) {
    tone(sound_out,7000, 1000);
    delay(500);
    tone(sound_out,3000, 1000);
    delay(500);
  } 

  digitalWrite(plant_pin, LOW);
  delay(5000);
}
