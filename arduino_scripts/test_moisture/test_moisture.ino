// https://www.instructables.com/id/Moisture-Detection-With-Two-Nails/
int plant_pin = 2;
int plant_read = 0;

int plant_value = 0;

void setup() {
  // put your setup code here, to run once:
  pinMode(plant_pin, OUTPUT);
  Serial.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly:
  digitalWrite(plant_pin, HIGH);
  delay(500);
  plant_value = analogRead(plant_read);
  Serial.println(plant_value);
  digitalWrite(plant_pin, LOW);
  delay(1000);
}
