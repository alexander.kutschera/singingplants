int sound_out = 0;

void setup() {
  pinMode(sound_out, OUTPUT);
}

void loop() {
  tone(sound_out,7000, 1000);
  delay(500);
  tone(sound_out,6000, 1000);
  delay(500);
  tone(sound_out,5000, 1000);
  delay(500);
  tone(sound_out,4000, 1000);
  delay(500);
  tone(sound_out,5000, 1000);
  delay(500);
  tone(sound_out,3000, 1000);
  delay(500);
  tone(sound_out,3500, 1000);
  delay(500);
}
