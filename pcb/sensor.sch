EESchema Schematic File Version 4
LIBS:power
LIBS:device
LIBS:74xx
LIBS:audio
LIBS:interface
LIBS:sensor-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Plant sensor"
Date "2018-12-08"
Rev "0.1"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L pspice:CAP C1
U 1 1 5C0C1595
P 7950 3450
F 0 "C1" H 8128 3496 50  0000 L CNN
F 1 "100nF" H 8128 3405 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 7950 3450 50  0001 C CNN
F 3 "" H 7950 3450 50  0001 C CNN
	1    7950 3450
	1    0    0    -1  
$EndComp
$Comp
L pspice:CAP C2
U 1 1 5C0C1806
P 4850 3650
F 0 "C2" H 5028 3696 50  0000 L CNN
F 1 "100nF" H 5028 3605 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 4850 3650 50  0001 C CNN
F 3 "" H 4850 3650 50  0001 C CNN
	1    4850 3650
	1    0    0    -1  
$EndComp
$Comp
L Device:R R4
U 1 1 5C0C1C0F
P 4200 3300
F 0 "R4" H 4270 3346 50  0000 L CNN
F 1 "22k" H 4270 3255 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 4130 3300 50  0001 C CNN
F 3 "~" H 4200 3300 50  0001 C CNN
	1    4200 3300
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D1
U 1 1 5C0C2364
P 5800 4000
F 0 "D1" H 5791 4216 50  0000 C CNN
F 1 "LED" H 5791 4125 50  0000 C CNN
F 2 "LED_THT:LED_D5.0mm" H 5800 4000 50  0001 C CNN
F 3 "~" H 5800 4000 50  0001 C CNN
	1    5800 4000
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R2
U 1 1 5C0C23EB
P 5800 3350
F 0 "R2" H 5870 3396 50  0000 L CNN
F 1 "100" H 5870 3305 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 5730 3350 50  0001 C CNN
F 3 "~" H 5800 3350 50  0001 C CNN
	1    5800 3350
	1    0    0    -1  
$EndComp
$Comp
L LED:WS2812B D4
U 1 1 5C0C29E3
P 3500 3750
F 0 "D4" V 3546 3409 50  0000 R CNN
F 1 "WS2812B" V 3455 3409 50  0000 R CNN
F 2 "LED_SMD:LED_WS2812B_PLCC4_5.0x5.0mm_P3.2mm" H 3550 3450 50  0001 L TNN
F 3 "https://cdn-shop.adafruit.com/datasheets/WS2812B.pdf" H 3600 3375 50  0001 L TNN
	1    3500 3750
	0    1    1    0   
$EndComp
$Comp
L LED:WS2812B D3
U 1 1 5C0C2A99
P 3500 4350
F 0 "D3" V 3546 4009 50  0000 R CNN
F 1 "WS2812B" V 3455 4009 50  0000 R CNN
F 2 "LED_SMD:LED_WS2812B_PLCC4_5.0x5.0mm_P3.2mm" H 3550 4050 50  0001 L TNN
F 3 "https://cdn-shop.adafruit.com/datasheets/WS2812B.pdf" H 3600 3975 50  0001 L TNN
	1    3500 4350
	0    1    1    0   
$EndComp
$Comp
L Device:R R6
U 1 1 5C0C305B
P 3500 3300
F 0 "R6" H 3570 3346 50  0000 L CNN
F 1 "100" H 3570 3255 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 3430 3300 50  0001 C CNN
F 3 "~" H 3500 3300 50  0001 C CNN
	1    3500 3300
	-1   0    0    1   
$EndComp
Wire Wire Line
	3200 4350 3200 4050
$Comp
L Device:Buzzer BZ1
U 1 1 5C0C4CB5
P 5500 2400
F 0 "BZ1" H 5653 2429 50  0000 L CNN
F 1 "Buzzer" H 5653 2338 50  0000 L CNN
F 2 "Buzzer_Beeper:Buzzer_12x9.5RM7.6" V 5475 2500 50  0001 C CNN
F 3 "~" V 5475 2500 50  0001 C CNN
	1    5500 2400
	1    0    0    -1  
$EndComp
$Comp
L Device:R R3
U 1 1 5C0C4D65
P 6150 3250
F 0 "R3" H 6220 3296 50  0000 L CNN
F 1 "100" H 6220 3205 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 6080 3250 50  0001 C CNN
F 3 "~" H 6150 3250 50  0001 C CNN
	1    6150 3250
	1    0    0    -1  
$EndComp
$Comp
L Device:R_POT RV1
U 1 1 5C0C7D65
P 5200 5250
F 0 "RV1" H 5130 5296 50  0000 R CNN
F 1 "R_POT" H 5130 5205 50  0000 R CNN
F 2 "Potentiometer_SMD:Potentiometer_Bourns_3214J_Horizontal" H 5200 5250 50  0001 C CNN
F 3 "~" H 5200 5250 50  0001 C CNN
	1    5200 5250
	0    1    1    0   
$EndComp
$Comp
L Device:R R1
U 1 1 5C0CAD26
P 5000 4600
F 0 "R1" H 4930 4554 50  0000 R CNN
F 1 "10k" H 4930 4645 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 4930 4600 50  0001 C CNN
F 3 "~" H 5000 4600 50  0001 C CNN
	1    5000 4600
	-1   0    0    1   
$EndComp
$Comp
L sensor-rescue:SJ-3524-SMT-SJ-3524-SMT J2
U 1 1 5C167B3F
P 4450 4250
F 0 "J2" H 4120 4254 50  0000 R CNN
F 1 "SJ-3524-SMT" H 4120 4345 50  0000 R CNN
F 2 "SJ-3524-SMT:CUI_SJ-3524-SMT" H 4450 4250 50  0001 L BNN
F 3 "Unavailable" H 4450 4250 50  0001 L BNN
F 4 "None" H 4450 4250 50  0001 L BNN "Feld4"
F 5 "None" H 4450 4250 50  0001 L BNN "Feld5"
F 6 "SJ-3524-SMT-TR" H 4450 4250 50  0001 L BNN "Feld6"
F 7 "SJ Series 3.5 mm 12 V SMT Right Angle Stereo Audio Jack" H 4450 4250 50  0001 L BNN "Feld7"
F 8 "https://www.cui.com/product/interconnect/audio-connectors/3.5-mm-jacks/sj-352x-smt-series?utm_source=snapeda.com&utm_medium=referral&utm_campaign=snapedaBOM" H 4450 4250 50  0001 L BNN "Feld8"
F 9 "CUI Inc." H 4450 4250 50  0001 L BNN "Feld9"
	1    4450 4250
	-1   0    0    1   
$EndComp
$Comp
L sensor-rescue:1825910-7-1825910-7 S1
U 1 1 5C165B42
P 2500 3650
F 0 "S1" V 2454 3880 50  0000 L CNN
F 1 "1825910-7" V 2545 3880 50  0000 L CNN
F 2 "1825910-7:SW_1825910-7" H 2500 3650 50  0001 L BNN
F 3 "TE Connectivity" H 2500 3650 50  0001 L BNN
F 4 "https://www.te.com/usa-en/product-1825910-7.html?te_bu=Cor&te_type=disp&te_campaign=seda_glo_cor-seda-global-disp-prtnr-fy19-seda-model-bom-cta_sma-317_1&elqCampaignId=32493" H 2500 3650 50  0001 L BNN "Feld4"
F 5 "None" H 2500 3650 50  0001 L BNN "Feld5"
F 6 "Switch Tactile OFF _ON_ SPST Round Button PC Pins 0.05A 24VDC 100000Cycle 2.55N Thru-Hole Loose Piece" H 2500 3650 50  0001 L BNN "Feld6"
F 7 "1825910-7" H 2500 3650 50  0001 L BNN "Feld7"
F 8 "Unavailable" H 2500 3650 50  0001 L BNN "Feld8"
F 9 "None" H 2500 3650 50  0001 L BNN "Feld9"
F 10 "1825910-7" H 2500 3650 50  0001 L BNN "Feld10"
	1    2500 3650
	0    -1   -1   0   
$EndComp
Text Label 5150 5600 0    50   ~ 0
worm1
Text Label 5550 5600 0    50   ~ 0
worm2
$Comp
L sensor-rescue:PCM12SMTR-PCM12SMTR SW1
U 1 1 5C238C31
P 6650 3350
F 0 "SW1" V 6696 3022 50  0000 R CNN
F 1 "PCM12SMTR" V 6605 3022 50  0000 R CNN
F 2 "PCM12SMTR:SW_PCM12SMTR" H 6650 3350 50  0001 L BNN
F 3 "https://www.digikey.de/product-detail/en/c-k/PCM12SMTR/401-2016-1-ND/1640125?utm_source=snapeda&utm_medium=aggregator&utm_campaign=symbol" H 6650 3350 50  0001 L BNN
F 4 "DO-201 C&amp;K Components" H 6650 3350 50  0001 L BNN "Feld4"
F 5 "PCM12SMTR" H 6650 3350 50  0001 L BNN "Feld5"
F 6 "401-2016-1-ND" H 6650 3350 50  0001 L BNN "Feld6"
F 7 "Switch Slide ON ON SPDT Side Slide 0.3A 6VDC 10000Cycles Gull Wing SMD T/R" H 6650 3350 50  0001 L BNN "Feld7"
F 8 "C&K" H 6650 3350 50  0001 L BNN "Feld8"
	1    6650 3350
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R5
U 1 1 5C0C1C9C
P 4350 3650
F 0 "R5" V 4143 3650 50  0000 C CNN
F 1 "22k" V 4234 3650 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 4280 3650 50  0001 C CNN
F 3 "~" H 4350 3650 50  0001 C CNN
	1    4350 3650
	0    1    1    0   
$EndComp
$Comp
L sensor-rescue:ATtiny85-20PU-MCU_Microchip_ATtiny U1
U 1 1 5C0C143D
P 1700 2950
F 0 "U1" H 1170 2996 50  0000 R CNN
F 1 "ATtiny85-20PU" H 1170 2905 50  0000 R CNN
F 2 "Package_DIP:DIP-8_W7.62mm" H 1700 2950 50  0001 C CIN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/atmel-2586-avr-8-bit-microcontroller-attiny25-attiny45-attiny85_datasheet.pdf" H 1700 2950 50  0001 C CNN
	1    1700 2950
	1    0    0    -1  
$EndComp
$Comp
L sensor-rescue:BAT-HLD-001-BAT-HLD-001 U2
U 1 1 5C2389FB
P 6750 3850
F 0 "U2" H 6750 3850 50  0001 L BNN
F 1 "BAT-HLD-001" H 6750 3850 50  0001 L BNN
F 2 "BAT-HLD-001:BAT-HLD-001" H 6750 3850 50  0001 L BNN
F 3 "None" H 6750 3850 50  0001 L BNN
F 4 "None" H 6750 3850 50  0001 L BNN "Feld4"
F 5 "BAT-HLD-001-TR" H 6750 3850 50  0001 L BNN "Feld5"
F 6 "Unavailable" H 6750 3850 50  0001 L BNN "Feld6"
F 7 "Linx Technologies" H 6750 3850 50  0001 L BNN "Feld7"
F 8 "CR2032 Battery Holder Surface Mount Tape and Reel" H 6750 3850 50  0001 L BNN "Feld8"
	1    6750 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	6750 4050 6750 4400
Wire Wire Line
	6750 4800 6150 4800
Wire Wire Line
	6650 2950 6650 2700
Wire Wire Line
	7950 3200 7950 2700
Wire Wire Line
	7950 2700 6650 2700
Connection ~ 6650 2700
Wire Wire Line
	6650 2700 6650 2200
Wire Wire Line
	7950 3700 7950 4400
Wire Wire Line
	7950 4400 6750 4400
Connection ~ 6750 4400
Wire Wire Line
	6750 4400 6750 4800
Wire Wire Line
	5150 5600 5150 5400
Wire Wire Line
	5150 5400 5200 5400
Wire Wire Line
	5000 4450 5350 4450
Wire Wire Line
	5150 2300 5400 2300
Wire Wire Line
	6150 3400 6150 4800
Wire Wire Line
	5800 3500 5800 3850
Wire Wire Line
	5800 4150 5800 4800
Connection ~ 6150 4800
Wire Wire Line
	5800 4800 6150 4800
Connection ~ 5350 4450
Wire Wire Line
	5350 4450 5350 5250
Connection ~ 5800 4800
Wire Wire Line
	4850 3900 3950 3900
Wire Wire Line
	3950 3900 3950 4150
Wire Wire Line
	3950 4450 3950 4800
Connection ~ 3950 4800
Wire Wire Line
	1700 2350 1700 2200
Wire Wire Line
	1700 4800 2400 4800
Wire Wire Line
	1700 3550 1700 4800
Wire Wire Line
	5150 2300 5150 2650
Wire Wire Line
	2300 2650 5150 2650
Wire Wire Line
	5800 2750 5800 3200
Wire Wire Line
	2300 2750 5550 2750
Wire Wire Line
	5550 2750 5550 5600
Connection ~ 5550 2750
Wire Wire Line
	5550 2750 5800 2750
Wire Wire Line
	2300 2850 5350 2850
Wire Wire Line
	5350 2850 5350 4450
Wire Wire Line
	4850 2950 4850 3300
Wire Wire Line
	2300 2950 4850 2950
Wire Wire Line
	5400 2500 5400 2600
Wire Wire Line
	5400 2600 6150 2600
Wire Wire Line
	6150 2600 6150 3100
Wire Wire Line
	2300 3050 3500 3050
Wire Wire Line
	3500 3050 3500 3150
Wire Wire Line
	3200 4050 2750 4050
Wire Wire Line
	2750 4050 2750 4800
Connection ~ 3200 4050
Wire Wire Line
	3200 4050 3200 3750
Connection ~ 2750 4800
Wire Wire Line
	2750 4800 3850 4800
Wire Wire Line
	3800 3750 3800 4350
Wire Wire Line
	3950 4800 5000 4800
Wire Wire Line
	5000 4750 5000 4800
Connection ~ 5000 4800
Wire Wire Line
	5000 4800 5800 4800
Wire Wire Line
	2300 3150 2400 3150
Wire Wire Line
	2400 3150 2400 3250
Wire Wire Line
	2400 4050 2400 4800
Connection ~ 2400 4800
Wire Wire Line
	2400 4800 2750 4800
Wire Wire Line
	4200 3150 4200 2200
Connection ~ 4200 2200
Wire Wire Line
	4200 2200 6650 2200
Wire Wire Line
	4200 3650 3850 3650
Wire Wire Line
	3850 3650 3850 4800
Connection ~ 3850 4800
Wire Wire Line
	3850 4800 3950 4800
Wire Wire Line
	4500 3650 4500 3450
Wire Wire Line
	4500 3450 4200 3450
Wire Wire Line
	4500 3450 4500 3300
Wire Wire Line
	4500 3300 4850 3300
Connection ~ 4500 3450
Connection ~ 4850 3300
Wire Wire Line
	4850 3300 4850 3400
Wire Wire Line
	3800 3750 3800 2200
Wire Wire Line
	1700 2200 2600 2200
Connection ~ 3800 3750
Connection ~ 3800 2200
Wire Wire Line
	3800 2200 4200 2200
Wire Wire Line
	2600 2250 2600 2200
Connection ~ 2600 2200
Wire Wire Line
	2600 2200 3800 2200
Wire Wire Line
	2600 2550 2600 3250
$Comp
L Device:R R7
U 1 1 5C9D6F8A
P 2600 2400
F 0 "R7" H 2530 2354 50  0000 R CNN
F 1 "10k" H 2530 2445 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 2530 2400 50  0001 C CNN
F 3 "~" H 2600 2400 50  0001 C CNN
	1    2600 2400
	-1   0    0    1   
$EndComp
$EndSCHEMATC
